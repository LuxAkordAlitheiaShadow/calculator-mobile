package com.lux.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.view.View

class MainActivity : AppCompatActivity() {

    private var firstValue : Float = 0.0f
    private var operator : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Text initialization
        val text_result = findViewById<TextView>(R.id.text_result)

        // Buttons initialization
        val btn_num_0 = findViewById<Button>(R.id.btn_num_0)
        val btn_num_1 = findViewById<Button>(R.id.btn_num_1)
        val btn_num_2 = findViewById<Button>(R.id.btn_num_2)
        val btn_num_3 = findViewById<Button>(R.id.btn_num_3)
        val btn_num_4 = findViewById<Button>(R.id.btn_num_4)
        val btn_num_5 = findViewById<Button>(R.id.btn_num_5)
        val btn_num_6 = findViewById<Button>(R.id.btn_num_6)
        val btn_num_7 = findViewById<Button>(R.id.btn_num_7)
        val btn_num_8 = findViewById<Button>(R.id.btn_num_8)
        val btn_num_9 = findViewById<Button>(R.id.btn_num_9)
        val btn_equal = findViewById<Button>(R.id.btn_equal)
        val btn_add = findViewById<Button>(R.id.btn_add)
        val btn_sub = findViewById<Button>(R.id.btn_sub)
        val btn_mult = findViewById<Button>(R.id.btn_mult)
        val btn_div = findViewById<Button>(R.id.btn_div)
        val btn_clear = findViewById<Button>(R.id.btn_clear)
        val btn_dot = findViewById<Button>(R.id.btn_dot)
        val btn_opposite = findViewById<Button>(R.id.btn_opposite)

        // Number button listener
        btn_num_0.setOnClickListener()
        {
            text_result.append("0")
        }
        btn_num_1.setOnClickListener()
        {
            text_result.append("1")
        }
        btn_num_2.setOnClickListener()
        {
            text_result.append("2")
        }
        btn_num_3.setOnClickListener()
        {
            text_result.append("3")
        }
        btn_num_4.setOnClickListener()
        {
            text_result.append("4")
        }
        btn_num_5.setOnClickListener()
        {
            text_result.append("5")
        }
        btn_num_6.setOnClickListener()
        {
            text_result.append("6")
        }
        btn_num_7.setOnClickListener()
        {
            text_result.append("7")
        }
        btn_num_8.setOnClickListener()
        {
            text_result.append("8")
        }
        btn_num_9.setOnClickListener()
        {
            text_result.append("9")
        }
        
        // Operator button listener
        btn_equal.setOnClickListener()
        {
            val input = text_result.text.toString().toFloat()
            when (operator)
            {
                "+" -> text_result.setText( (firstValue + input).toString() )
                "-" -> text_result.setText( (firstValue - input).toString() )
                "*" -> text_result.setText( (firstValue * input).toString() )
                "/" -> text_result.setText( (firstValue / input).toString() )
            }

        }

        btn_add.setOnClickListener()
        {
            operator = "+"
            firstValue = text_result.text.toString().toFloat()
            text_result.setText("")
        }

        btn_sub.setOnClickListener()
        {
            operator = "-"
            firstValue = text_result.text.toString().toFloat()
            text_result.setText("")
        }

        btn_mult.setOnClickListener()
        {
            operator = "*"
            firstValue = text_result.text.toString().toFloat()
            text_result.setText("")
        }

        btn_div.setOnClickListener()
        {
            operator = "/"
            firstValue = text_result.text.toString().toFloat()
            text_result.setText("")
        }

        btn_clear.setOnClickListener()
        {
            operator = ""
            firstValue = 0.0f
            text_result.setText("")
        }

        btn_opposite.setOnClickListener()
        {
            // Number is negative statement
            if ( text_result.text.toString()[0] == '-' )
            {
                text_result.setText(text_result.text.toString().replace("-",""))
            }
            // Number is positive statement
            else
            {
                text_result.setText("-"+text_result.text.toString())
            }
        }

        btn_dot.setOnClickListener()
        {
            text_result.append(".")
        }
    }
}